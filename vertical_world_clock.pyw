from tkinter import *
import tkinter as tk
import tkinter.ttk as ttk
import datetime
import time
import os

# Build the window
top = Tk()
top.title("Clock")
top.resizable(0, 0)
# Comment out this next line if you don't want this to stay on top of all other windows
# top.call('wm', 'attributes', '.', '-topmost', '1')


# Setup the time offsets and date/time formatting
formated_windows = '%#m/%#d/%Y\n%#I:%M:%S %p'
formated_unix = '%-m/%-d/%Y\n%-I:%M:%S %p'
zone_name = ['Eastern', 'Central', 'Mountain', 'Pacific', 'Alaska', 'Hawaii', 'Guam']
time_offset = [-4, -5, -6, -7, -8, -10, 10]
labelList = []


def build_ui():
	grid = 0
	for zone in zone_name:
		index = zone_name.index(zone)
		zone_title = Label(top, font=('times', 15), text=zone_name[index])
		zone_title.grid(row=grid, column=0, columnspan=1, sticky='ew')
		labelList.append(zone_title)
		grid += 1
		zone_clock = Label(top, font=('times', 20, 'bold'))
		zone_clock.grid(row=grid, column=0, columnspan=1, sticky='ew')
		labelList.append(zone_clock)
		grid += 1
		if index < (len(zone_name)-1):
			ttk.Separator(top, orient=HORIZONTAL).grid(row=grid, column=0, columnspan=1, sticky='ew')
			grid += 1
		else:
			None


def tick():
	index = 0
	for zone in zone_name:
		offset = int(index/2)
		adjusted_time = datetime.timezone(datetime.timedelta(hours=time_offset[offset]))
		new_zone = datetime.datetime.now(adjusted_time)
		if os.name == 'nt':
			zone_time = new_zone.strftime(formated_windows)
		else:
			zone_time = new_zone.strftime(formated_unix)
		time_check = new_zone.strftime('%I:%M:%S')
		if time_check == time.strftime('%I:%M:%S'):
			labelList[index].config(bg='green')
			labelList[index+1].config(bg='green')
		else:
			labelList[index].config(bg='#f0f0f0')
			labelList[index+1].config(bg='#f0f0f0')
		labelList[index+1].config(text=zone_time)
		index += 2
	top.after(200, tick)


build_ui()	
tick()
top.mainloop()
